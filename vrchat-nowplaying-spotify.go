package main

import (
	"fmt"
	"syscall"

	"github.com/JamesHovious/w32"
	"github.com/hypebeast/go-osc/osc"
	"golang.org/x/sys/windows"
)

func main() {
	client := osc.NewClient("127.0.0.1", 9000)
	msg := osc.NewMessage("/chatbox/input")
	p := findProcessByName("Spotify.exe")
	str := "🎵 " + getWindowTitle(p.ProcessID)
	fmt.Println(str)
	msg.Append(str, true)
	client.Send(msg)
}

func findProcessByName(name string) *w32.PROCESSENTRY32 {
	snapshot := w32.CreateToolhelp32Snapshot(w32.TH32CS_SNAPPROCESS, 0)
	var pe w32.PROCESSENTRY32
	if w32.Process32First(snapshot, &pe) {
		for w32.Process32Next(snapshot, &pe) {
			if windows.UTF16ToString(pe.ExeFile[:]) == name {
				return &pe
			}
		}
	}
	return nil
}

func getWindowTitle(pid uint32) string {
	var title string
	cb := syscall.NewCallback(func(h syscall.Handle, p uintptr) uintptr {
		_, t := w32.GetWindowThreadProcessId(w32.HWND(h))
		if uint32(t) == pid {
			t := w32.GetWindowText(w32.HWND(h))
			if w32.IsWindowVisible(w32.HWND(h)) {
				title = t
				return 0
			}
		}
		return 1
	})
	_, _, _ = syscall.
		NewLazyDLL("user32.dll").
		NewProc("EnumWindows").
		Call(cb)
	return title
}
